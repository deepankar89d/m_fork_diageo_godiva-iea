<%--

  <viewFormat.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for view format dropdown.

  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>

[
{
 "text":"1 Column",
 "value":"1"
 },
{
 "text":"3 Column",
 "value":"3"
 },
{
 "text":"4 Column",
 "value":"4"
 }
]